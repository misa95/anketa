<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    const TYPE_TEACHER   = 'teacher';
    const TYPE_SUBJECT   = 'subject';
    const TYPE_OTHER     = 'other';

    const TYPE_OPTIONS = [
        self::TYPE_TEACHER => 'O наставнику',
        self::TYPE_SUBJECT => 'O предмету',
        self::TYPE_OTHER   => 'O школи',
    ];

    protected $fillable = [
        'text', 'type'
    ];

    public function isTeacher()
    {
        return $this->type === self::TYPE_TEACHER;
    }

    public function isSubject()
    {
        return $this->type === self::TYPE_SUBJECT;
    }

    public function isOther()
    {
        return $this->type === self::TYPE_OTHER;
    }
}
