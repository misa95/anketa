<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'settings';

    protected $fillable = ['name'];

    public static function set(string $name, $value)
    {
        $setting = static::firstOrNew([
            'name' => $name
        ]);

        $setting->value = $value;

        return tap($setting)->save();
    }

    public static function get(string $name)
    {
        $setting = self::firstOrNew([
            'name' => $name
        ]);

        return $setting->value;
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = serialize($value);
    }

    public function getValueAttribute($value)
    {
        if ($value) {
            return unserialize($value);
        }
    }
}
