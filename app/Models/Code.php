<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Code extends Authenticatable
{
    const CODES_PUBLIC_SETTING = 'codes-public';

    protected $fillable = [
        'code', 'semester'
    ];

    protected $with = [
        'department.subjects.teacher', 'department.subjects.assistant',
    ];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function getAuthIdentifierName()
    {
        return 'code';
    }

    public function scopeAvailable(Builder $query)
    {
        $query->where('codes.used', false);
    }

    public function scopeUsed(Builder $query)
    {
        $query->where('codes.used', true);
    }

    public function scopeForSemester(Builder $query, $semester)
    {
        return $query->where('semester', $semester);
    }
}
