<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerSheet extends Model
{
    public $timestamps = false;
}
