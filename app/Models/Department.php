<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'name', 'shorthand', 'total_students',
    ];

    public function subjects()
    {
        return $this->hasMany(Subject::class);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function allTeachers()
    {
        return $this->subjects->pluck('teacher')->concat(
            $this->subjects->pluck('assistant')
        )->reject(function ($teacher) {
            return $teacher === null;
        })->unique->id;
    }

    public function availableSemesters()
    {
        return $this->subjects()->distinct()->pluck('semester');
    }

    public function codes()
    {
        return $this->hasMany(Code::class);
    }
}