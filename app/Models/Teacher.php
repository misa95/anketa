<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'name',
    ];

    public function subjectsTaught()
    {
        return $this->hasMany(Subject::class, 'teacher_id');
    }

    public function subjectsAssissted()
    {
        return $this->hasMany(Subject::class, 'assistant_id');
    }

    public function hasSubjects()
    {
        return ($this->subjectsTaught()->count() + $this->subjectsAssissted()->count()) > 0;
    }
}
