<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SubjectRequest;
use App\Models\Department;
use App\Models\Subject;
use App\Models\Teacher;

class SubjectController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Department $department)
    {
        $subject = new Subject();
        $subject->department = $department;
        $teachers = $this->prepareTeacherOptions();

        return view('admin.subjects.create', compact('subject', 'department', 'teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\SubjectRequest $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SubjectRequest $request, Department $department)
    {
        $subject = Subject::make($request->validated());
        $subject->department_id = $department->id;

        $subject->save();

        return redirect()->route('admin.departments.show', ['department' => $department->id])->with('success', 'Predmet uspešno dodat.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Subject $subject
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Subject $subject)
    {
        $teachers = $this->prepareTeacherOptions();

        return view('admin.subjects.edit', compact('subject', 'teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\SubjectRequest $request
     * @param \App\Models\Subject $subject
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SubjectRequest $request, Subject $subject)
    {
        $subject->update($request->validated());

        return redirect()->route('admin.departments.show', $subject->department_id)->with('success', 'Predmet uspešno izmenjen.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Subject $subject
     * @return void
     */
    public function destroy(Subject $subject)
    {
        $subject->delete();

        return redirect()->route('admin.departments.show', $subject->department_id)->with('success', 'Predmet uspešno izbrisan.');
    }

    /**
     * @return array
     */
    protected function prepareTeacherOptions()
    {
        return Teacher::all()->mapWithKeys(function (Teacher $teacher) {
            return [$teacher->id => $teacher->name];
        })->sort()->toArray();
    }
}
