<?php

namespace App\Http\Controllers\Admin;

use App\Facades\ReportManager;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = ReportManager::all();

        return view('admin.reports.index', compact('reports'));
    }

    /**
     * Display the specified resource.
     *
     * @param string $reportName
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function show(string $reportName)
    {
        return ReportManager::download($reportName);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Respons
     */
    public function store(Request $request)
    {
        return ReportManager::generateAndDownload();
    }

    public function reset()
    {
        ReportManager::clear();

        return redirect()->route('admin.reports.index')
            ->with('success', 'Анкета је ресетована.');
    }

    public function destroy()
    {
        return ReportManager::archive();
    }
}
