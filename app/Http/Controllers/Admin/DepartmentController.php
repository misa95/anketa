<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DepartmentRequest;
use App\Http\Requests\Admin\ImportSubjectsRequest;
use App\Imports\DepartmentSubjectsImport;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $request->session()->reflash();

        $department = Department::first();

        if ($department === null) {
            return redirect()->route('admin.departments.create');
        }

        return redirect()->route('admin.departments.show', $department->id);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Department $department)
    {
        $semesters = $department->subjects()
                                ->with('teacher', 'assistant')
                                ->get()
                                ->groupBy('semester')
                                ->sortKeys();

        $departments = Department::all();

        return view('admin.departments.show', compact('department', 'departments', 'semesters'));
    }

    public function create()
    {
        return view('admin.departments.create');
    }

    public function store(DepartmentRequest $request)
    {
        $department = Department::create($request->validated());

        return redirect()->route('admin.departments.show', $department->id)
            ->with('success', 'Smer je uspešno dodat.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Department $department)
    {
        return view('admin.departments.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\DepartmentRequest $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DepartmentRequest $request, Department $department)
    {
        $department->update($request->validated());

        return redirect()->route('admin.departments.show', $department->id)
            ->with('success', 'Smer uspešno izmenjen.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Department $department)
    {
        try {
            $department->delete();
        } catch (\Exception $e) {
            return redirect()->route('admin.departments.edit', $department)
                ->with('error', 'Ne možete izbrisati smer dok anketa nije arhivirana.');
        }

        return redirect()->route('admin.departments.index')
            ->with('success', 'Smer uspešno izbrisan.');
    }

    public function import(Department $department, ImportSubjectsRequest $request)
    {
        Excel::import($import = DepartmentSubjectsImport::forDepartment($department), $request->file('excel'));

        DB::transaction(function () use ($import, $department) {
            $department->subjects()->delete();

            DB::table('subjects')->insert($import->subjects);
        });

        return redirect()->route('admin.departments.show', $department->id);
    }
}
