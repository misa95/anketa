<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TeacherRequest;
use App\Models\Subject;
use App\Models\Teacher;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::orderBy('name', 'asc')->get();

        return view('admin.teachers.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.teachers.create', [
            'teacher' => new Teacher(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherRequest $request)
    {
        Teacher::create($request->validated());

        return redirect()->route('admin.teachers.index')->with('success', 'Profesor uspešno kreiran.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        $teacher->load('subjectsTaught.department', 'subjectsAssissted.department');

        $subjects = $teacher->subjectsAssissted
                            ->merge($teacher->subjectsTaught)
                            ->transform(function (Subject $subject) use ($teacher) {
                                $subject->role = ($subject->teacher_id == $teacher->id)
                                    ? 'Profesor'
                                    : 'Asistent';

                                return $subject;
                            })
                            ->sortBy('name');

        return view('admin.teachers.show', compact('teacher', 'subjects'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('admin.teachers.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherRequest $request, Teacher $teacher)
    {
        $teacher->update($request->validated());

        return redirect()->route('admin.teachers.index')->with('success', 'Profesor je uspešno izmenjen.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        $redirectResponse = redirect()->route('admin.teachers.index');

        if ($teacher->hasSubjects()) {
            $redirectResponse->with('error', 'Ne možete izbrisati profesora koji ima predmete.');
        } else {
            $teacher->delete();

            $redirectResponse->with('success', 'Profesor uspešno izbrisan.');
        }

        return $redirectResponse;
    }
}
