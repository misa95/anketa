<?php

namespace App\Http\Controllers;

use App\Facades\UserSurvey;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (UserSurvey::isOngoing()) {
            return UserSurvey::redirectToNextQuestion();
        }

        $department = Auth::user()->department;

        return view('home', compact('department'));
    }
}
