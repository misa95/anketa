<?php

namespace App\Http\Requests;

use App\Models\Question;
use Illuminate\Foundation\Http\FormRequest;

class QuestionAnswerRequest extends FormRequest
{
    protected $markValidation =
        'required|numeric|min:1|max:5';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $question = Question::findOrFail($this->route('question'));

        if ($question->isOther()) {
            return [
                'mark' => $this->markValidation,
            ];
        }

        $subjects = $this->user()->department->subjects()->where('semester', $this->user()->semester)->get();
        $rules = [];

        foreach ($subjects as $subject) {
            if ($question->isTeacher()) {
                $rules["subject-{$subject->id}-teacher-{$subject->teacher_id}"] = $this->markValidation;

                if ($subject->assistant) {
                    $rules["subject-{$subject->id}-assistant-{$subject->assistant_id}"] = $this->markValidation;
                }

                continue;
            }

            $rules["subject-{$subject->id}"] = $this->markValidation;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            '*.required' => 'Moraš izabrati ocenu.'
        ];
    }
}
