<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'semester' => 'required|numeric',
            'teacher_id' => 'required|numeric|bail|'
                . Rule::exists('teachers', 'id'),
            'assistant_id' => 'nullable|numeric|bail|'
                . Rule::exists('teachers', 'id'),
        ];
    }
}
