<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;

class PasswordProtected
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (
            $request->has('password') &&
            Hash::check($request->input('password'), $request->user()->password)
        ) {
            return $next($request);
        }

        return redirect()->back()
            ->with('error', 'Uneta lozinka nije ispravna');
    }
}
