<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ReportManager extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'report-manager';
    }
}