<?php

namespace App\Providers;

use App\Services\ReportManager;
use App\Services\UserSurvey;
use Illuminate\Database\Schema\Builder;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::defaultStringLength(191);

        $this->app->bind('report-manager', function (Application $app) {
            return new ReportManager($app->get('filesystem')->disk('public'));
        });

        $this->app->singleton('user-survey', function (Application $app) {
            return new UserSurvey($app->make('auth')->user(), $app->make('session.store'));
        });
    }
}
