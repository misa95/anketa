<?php

namespace App\Providers;

use App\ViewComposers\AdminActiveItemViewComposer;
use App\ViewComposers\QuestionTypeOptionsViewComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerAdminComposers();
    }

    protected function registerAdminComposers()
    {
        View::composer('layouts.admin', AdminActiveItemViewComposer::class);

        View::composer('admin.questions.partials.fields', QuestionTypeOptionsViewComposer::class);
    }
}