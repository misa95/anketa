<?php

namespace App\Services\Calculators\Department;

use App\Models\Department;
use App\Services\Calculators\AnswerSet;
use App\Services\Calculators\Calculator;

abstract class DepartmentCalculator extends Calculator
{
    /** @var \App\Models\Department  */
    protected $department;

    public function __construct(Department $department)
    {
        $this->department = $department;
    }

    public function setDepartment(Department $department)
    {
        $this->department = $department;
    }

    /**
     * @param \App\Services\Calculators\AnswerSet $answerSet
     * @return \Illuminate\Support\Collection
     */
    protected function getSheetsForDepartment(AnswerSet $answerSet)
    {
        if (!$this->department->relationLoaded('answerSheets')) {
            $this->department->setRelation('answerSheets', $answerSet->answerSheets->where('department_id', $this->department->id));
        }

        return $this->department->answerSheets;
    }

    /**
     * @param \App\Services\Calculators\AnswerSet $answerSet
     * @return \Illuminate\Support\Collection
     */
    protected function getAnswersForDepartment(AnswerSet $answerSet)
    {
        if (!$this->department->relationLoaded('answers')) {
            $answerSheetIds = $this->getSheetsForDepartment($answerSet)->pluck('id');
            $this->department->setRelation('answers', $answerSet->answers->whereIn('answer_sheet_id', $answerSheetIds));
        }

        return $this->department->answers;
    }
}