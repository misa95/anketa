<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;

class AverageMark extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        $questionIds = $answerSet->getEduQuestions()->pluck('id');
        $answers = $this->getAnswersForDepartment($answerSet);

        return $this->formatDecimal(
            $answers->whereIn('question_id', $questionIds)->average('mark')
        );
    }
}