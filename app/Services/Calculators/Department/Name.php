<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;

class Name extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return $this->department->name;
    }
}