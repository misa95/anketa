<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;

class TeacherCount extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        $subjects = $this->department->subjects;

        $teacherIds   = $subjects->pluck('teacher_id');
        $assistantIds = $subjects->where('assistant_id', '!==', null)
                                 ->pluck('assistant_id');

        return $teacherIds->concat($assistantIds)->unique()->count();
    }
}