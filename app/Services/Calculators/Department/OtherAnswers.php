<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;
use Illuminate\Support\Collection;

class OtherAnswers extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        $questionIds = $answerSet->getOtherQuestions()->pluck('id');
        $answersByQuestionId = $this->getAnswersForDepartment($answerSet)->whereIn('question_id', $questionIds)->groupBy->question_id;

        return $answersByQuestionId->map(function (Collection $questionAnswers) {
            return $this->formatDecimal($questionAnswers->average->mark);
        });
    }
}