<?php

namespace App\Services\Calculators\Department;

use App\Models\Question;
use App\Models\Subject;
use App\Models\Teacher;
use App\Services\Calculators\AnswerSet;

class YearsOverview extends DepartmentCalculator
{
    /** @var AnswerSet */
    protected $answerSet;

    public function calculate(AnswerSet $answerSet)
    {
        $years = [];
        $this->answerSet = $answerSet;

        foreach ($this->department->availableSemesters() as $semester) {
            $years[] = $this->prepareOverviewForSemester($semester);
        }

        return collect($years);
    }

    /**
     * @param int $semester
     * @return \Illuminate\Support\Collection
     */
    protected function prepareOverviewForSemester(int $semester)
    {
        $data = [];

        $data['year'] = $this->convertSemesterToYear($semester);
        $data['surveyed'] = $this->getSurveyedStudentsForSemester($semester);
        $data['teacherCount'] = $this->getTeacherCountForSemester($semester);
        $data['teacherAverage'] = $this->getTeacherAverageForSemester($semester);
        $data['subjectCount'] = $this->getSubjectCountForSemester($semester);
        $data['subjects'] = $this->getSemesterSubjectOverview($semester);
        $data['markCount'] = $this->getMarkCountForSemester($semester);
        $data['otherAnswers'] = $this->getOtherAnswersForSemester($semester);

        return collect($data);
    }

    /**
     * @param int $semester
     * @return int
     */
    protected function convertSemesterToYear(int $semester)
    {
        return (int) ceil($semester / 2);
    }

    /**
     * @param int $semester
     * @return int
     */
    protected function getSurveyedStudentsForSemester(int $semester)
    {
        return $this->getSheetsForDepartment($this->answerSet)
                    ->where('semester', $semester)
                    ->count();
    }

    /**
     * @param int $semester
     * @return int
     */
    protected function getTeacherCountForSemester(int $semester)
    {
        $subjects = $this->department->subjects;

        $teacherIds   = $subjects->pluck('teacher_id');
        $assistantIds = $subjects->where('assistant_id', '!==', null)
                               ->pluck('assistant_id');

        return $teacherIds->concat($assistantIds)->unique()->count();
    }

    /**
     * @param int $semester
     * @return \Illuminate\Support\Collection
     */
    protected function getTeacherAverageForSemester(int $semester)
    {
        $questionIds = $this->answerSet->getTeacherQuestions()->pluck('id');
        $answerSheetIds = $this->getSheetsForDepartment($this->answerSet)->where('semester', $semester)->pluck('id');

        $answers = $this->getAnswersForDepartment($this->answerSet)
                        ->whereIn('question_id', $questionIds)
                        ->whereIn('answer_sheet_id', $answerSheetIds);

        $percentage = $answers->average->mark;

        return collect([
            'numeric' => $this->formatDecimal($percentage),
            'word'    => $this->convertMarkToWord($percentage),
        ]);
    }

    /**
     * @param int $semester
     * @return int
     */
    protected function getSubjectCountForSemester(int $semester)
    {
        return $this->getSubjectsForSemester($semester)->count();
    }

    protected function getSemesterSubjectOverview(int $semester)
    {
        return $this->getSubjectsForSemester($semester)->map(function (Subject $subject) {
            $subjectData = [
                'name' => $subject->name,
                'average' => $this->getSubjectAverage($subject),
                'teacher' => $subject->teacher->name,
                'teacherAverage' => $this->getSubjectTeacherAverage($subject, $subject->teacher),
                'assistant' => null,
                'assistantAverage' => null,
            ];

            if ($subject->assistant) {
                $subjectData['assistant'] = $subject->assistant->name;
                $subjectData['assistantAverage'] = $this->getSubjectTeacherAverage($subject, $subject->assistant);
            }

            return collect($subjectData);
        });
    }

    /**
     * @param int $semester
     * @return int
     */
    protected function getMarkCountForSemester(int $semester)
    {
        $answerSheetIds = $this->getSheetsForDepartment($this->answerSet)
                               ->where('semester', $semester)
                               ->pluck('id');

        $questionIds = $this->answerSet->getEduQuestions()->pluck('id');

        return $this->getAnswersForDepartment($this->answerSet)
                    ->whereIn('answer_sheet_id', $answerSheetIds)
                    ->whereIn('question_id', $questionIds)
                    ->count();
    }

    /**
     * @param int $semester
     * @return \Illuminate\Support\Collection
     */
    protected function getOtherAnswersForSemester(int $semester)
    {
        $questions = $this->answerSet->getOtherQuestions();
        $answerSheetIds = $this->getSheetsForDepartment($this->answerSet)
                       ->where('semester', $semester)
                       ->pluck('id');

        $answers = $this->getAnswersForDepartment($this->answerSet)
                        ->whereIn('answer_sheet_id', $answerSheetIds)
                        ->whereIn('question_id', $questions->pluck('id'));

        return $questions->keyBy('id')->map(function (Question $question) use ($answers) {
            return $this->formatDecimal(
                $answers->where('question_id', $question->id)->average->mark
            );
        });
    }

    /**
     * @param \App\Models\Subject $subject
     * @return string
     */
    protected function getSubjectAverage(Subject $subject)
    {
        return $this->formatDecimal(
            $this->getAnswersForDepartment($this->answerSet)
                 ->where('subject_id', $subject->id)
                 ->average
                 ->mark
        );
    }

    protected function getSubjectTeacherAverage(Subject $subject, Teacher $teacher)
    {
        return $this->formatDecimal(
            $this->getAnswersForDepartment($this->answerSet)
                 ->where('subject_id', $subject->id)
                 ->where('teacher_id', $teacher->id)
                 ->average
                 ->mark
        );
    }

    /**
     * @param int $semester
     * @return \App\Models\Subject[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    protected function getSubjectsForSemester(int $semester)
    {
        return $this->department
            ->subjects
            ->where('semester', $semester);
    }
}
