<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;

class TotalMarks extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return $this->getAnswersForDepartment($answerSet)->count();
    }
}