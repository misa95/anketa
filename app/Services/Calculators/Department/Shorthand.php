<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;

class Shorthand extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return $this->department->shorthand;
    }
}