<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;

class TeacherAverage extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        $answers = $this->getAnswersForDepartment($answerSet);
        $questionIds = $answerSet->getTeacherQuestions()->pluck('id');

        $percentage = $this->formatDecimal(
            $answers->whereIn('question_id', $questionIds)->average->mark
        );

        return collect([
            'numeric' => $percentage,
            'word'    => $this->convertMarkToWord($percentage),
        ]);
    }
}