<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;

class TotalStudents extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return $this->department->total_students;
    }
}