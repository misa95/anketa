<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;

class SubjectAverage extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        $questionIds = $answerSet->getSubjectQuestions()->pluck('id');
        $answers = $this->getAnswersForDepartment($answerSet)->whereIn('question_id', $questionIds);

        return $this->formatDecimal($answers->average->mark);
    }
}