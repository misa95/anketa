<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;

class SurveyedPercentage extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return $this->formatDecimal(
            ($this->getSheetsForDepartment($answerSet)->count() / $this->department->total_students) * 100
        );
    }
}