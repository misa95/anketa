<?php

namespace App\Services\Calculators\Department;

use App\Services\Calculators\AnswerSet;

class Surveyed extends DepartmentCalculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return $this->getSheetsForDepartment($answerSet)->count();
    }
}