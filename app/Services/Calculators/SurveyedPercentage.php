<?php

namespace App\Services\Calculators;

class SurveyedPercentage extends Calculator
{
    public function calculate(AnswerSet $answerSet)
    {
        $totalStudents = $answerSet->departments->sum->total_students;
        $totalSurveyed = $answerSet->answerSheets->count();

        return $this->formatDecimal(
            ($totalSurveyed / $totalStudents) * 100
        );
    }
}