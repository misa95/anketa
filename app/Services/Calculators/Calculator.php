<?php

namespace App\Services\Calculators;

abstract class Calculator
{
    protected $marks = [
        4.5 =>  'одличан',
        4   =>  'врлодобар',
        3.5 =>  'добар',
        3   =>  'довољан',
        0   =>  'недовољан',
    ];

    abstract public function calculate(AnswerSet $answerSet);

    protected function formatDecimal(float $decimal = null)
    {
        if ($decimal === null) {
            $decimal = 0;
        }

        return number_format($decimal, 2);
    }

    protected function convertMarkToWord(float $mark = null)
    {
        foreach ($this->marks as $min => $string) {
            if ($mark >= $min) {
                return $string;
            }
        }

        return last($this->marks);
    }
}
