<?php

namespace App\Services\Calculators;

use App\Models\Answer;
use App\Models\AnswerSheet;
use App\Models\Question;
use Illuminate\Support\Collection;

class AnswerSet
{
    /** @var \Illuminate\Support\Collection */
    public $answerSheets;

    /** @var \Illuminate\Support\Collection */
    public $answers;

    /** @var \Illuminate\Support\Collection */
    public $questions;

    /** @var \Illuminate\Support\Collection */
    public $departments;

    public function __construct()
    {
        $this->loadAnswerSheets()
             ->loadAnswers()
             ->loadQuestions();
    }

    protected function loadAnswerSheets()
    {
        $this->answerSheets = AnswerSheet::all();

        return $this;
    }

    protected function loadAnswers()
    {
        $this->answers = Answer::all();

        return $this;
    }

    protected function loadQuestions()
    {
        $this->questions = Question::all();

        return $this;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getOtherQuestions()
    {
        return $this->questions->where('type', Question::TYPE_OTHER);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getSubjectQuestions()
    {
        return $this->questions->where('type', Question::TYPE_SUBJECT);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getTeacherQuestions()
    {
        return $this->questions->where('type', Question::TYPE_TEACHER);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getEduQuestions()
    {
        return $this->getSubjectQuestions()->concat($this->getTeacherQuestions());
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getOtherAnswers()
    {
        return $this->answers->whereIn('question_id', $this->getOtherQuestions()->pluck('id'));
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getSubjectAnswers()
    {
        return $this->answers->whereIn('question_id', $this->getSubjectQuestions()->pluck('id'));
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getTeacherAnswers()
    {
        return $this->answers->whereIn('question_id', $this->getTeacherQuestions()->pluck('id'));
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getEduAnswers()
    {
        return $this->getSubjectAnswers()->concat($this->getTeacherAnswers());
    }

    /**
     * @param \Illuminate\Support\Collection $departments
     * @return $this
     */
    public function withDepartments(Collection $departments)
    {
        $this->departments = $departments;

        return $this;
    }
}