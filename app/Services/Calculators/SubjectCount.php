<?php

namespace App\Services\Calculators;

use App\Models\Subject;

class SubjectCount extends Calculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return Subject::count();
    }
}