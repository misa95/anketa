<?php

namespace App\Services\Calculators;

class DepartmentAverage extends Calculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return $this->formatDecimal(
            $answerSet->getEduAnswers()->average->mark
        );
    }
}