<?php

namespace App\Services\Calculators;

class SurveyedCount extends Calculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return $answerSet->answerSheets->count();
    }
}