<?php

namespace App\Services\Calculators;

class SubjectOverview extends Calculator
{
    public function calculate(AnswerSet $answerSet)
    {
        $percentage = $answerSet->getSubjectAnswers()->average->mark;
        return collect([
            'numeric' => $this->formatDecimal($percentage),
            'word'    => $this->convertMarkToWord($percentage),
        ]);
    }
}