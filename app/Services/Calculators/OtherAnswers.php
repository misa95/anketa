<?php

namespace App\Services\Calculators;

use App\Models\Question;

class OtherAnswers extends Calculator
{
    public function calculate(AnswerSet $answerSet)
    {
        $questions = $answerSet->getOtherQuestions();
        $answers = $answerSet->getOtherAnswers();

        return $questions->keyBy('id')->map(function (Question $question) use ($answers) {
            return $this->formatDecimal($answers->where('question_id', $question->id)->average->mark);
        });
    }
}