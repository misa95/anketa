<?php

namespace App\Services\Calculators;

class TeacherAverage extends Calculator
{
    public function calculate(AnswerSet $answerSet)
    {
        $teacherAverage = $answerSet->getTeacherAnswers()->average->mark;

        return collect([
            'numeric' => $this->formatDecimal($teacherAverage),
            'word'    => $this->convertMarkToWord($teacherAverage),
        ]);
    }
}