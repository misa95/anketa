<?php

namespace App\Services\Calculators;

class StudentTotal extends Calculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return $answerSet->departments->sum->total_students;
    }
}