<?php

namespace App\Services\Calculators;

class OtherQuestions extends Calculator
{
    public function calculate(AnswerSet $answerSet)
    {
        return $answerSet->getOtherQuestions();
    }
}