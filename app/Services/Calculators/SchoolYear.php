<?php

namespace App\Services\Calculators;

use Carbon\CarbonImmutable;

class SchoolYear extends Calculator
{
    public function calculate(AnswerSet $answerSet)
    {
        $now = new CarbonImmutable();

        return sprintf(
    "%s/%s",
            $now->subYear()->year,
            $now->year
        );
    }
}