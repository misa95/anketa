<?php

namespace App\Services;

use App\Models\AnswerSheet;
use App\Models\Code;
use App\Models\Question;
use Illuminate\Session\Store;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class UserSurvey
{
    /** @var Code */
    private $code;

    /** @var \App\Models\Department */
    private $department;

    /** @var Store */
    private $session;

    /** @var \Illuminate\Support\Collection */
    private $questions;

    /** @var \Illuminate\Support\Collection */
    private $answers;

    const KEY_QUESTIONS = 'user_survey_questions';
    const KEY_ANSWERS   = 'user_survey_answers';

    const KEY_STATUS = 'user_survey_status';
    const STATUS_NOT_STARTED = 'not_started';
    const STATUS_STARTED     = 'started';
    const STATUS_COMPLETED    = 'completed';

    public function __construct(Code $code, Store $session)
    {
        $this->code = $code;
        $this->department = $code->department;
        $this->session = $session;

        $this->load();
    }

    public function __destruct()
    {
        $this->persist();
    }

    private function load()
    {
        if (!$this->isOngoing()) {
            $this->session->put(self::KEY_STATUS, self::STATUS_NOT_STARTED);

            return false;
        }

        $this->questions = $this->session->get(self::KEY_QUESTIONS);
        $this->answers   = $this->session->get(self::KEY_ANSWERS);
    }

    public function hasNotStarted()
    {
        if (!$status = $this->getStatus()) {
            return false;
        }

        return $status === self::STATUS_NOT_STARTED;
    }

    public function hasStarted()
    {
        return !$this->hasNotStarted();
    }

    public function isOngoing()
    {
        return $this->getStatus() === self::STATUS_STARTED;
    }

    public function isCompleted()
    {
        return $this->getStatus() === self::STATUS_COMPLETED;
    }

    public function canBeCompleted()
    {
        return $this->answers->search(function (Collection $questionAnswers) {
            return $questionAnswers->isEmpty();
        }) === false;
    }

    public function getStatus()
    {
        return $this->session->get(self::KEY_STATUS);
    }

    /**
     * @return $this
     */
    public function start()
    {
        $this->session->put(self::KEY_STATUS, self::STATUS_STARTED);

        $this->questions = $this->prepareQuestions();
        $this->answers   = $this->prepareAnswers();

        $this->markCodeUsed();

        $this->persist();

        return $this;
    }

    /**
     * @param string $questionId
     * @return Question|null
     */
    public function getQuestion(string $questionId)
    {
        return $this->findQuestionById($questionId);
    }

    public function getQuestionAnswers(Question $question)
    {
        return $this->answers->get($question->id);
    }

    /**
     * @param \App\Models\Question $question
     * @return array|null
     */
    public function getAnswerToQuestion(Question $question)
    {
        return $this->answers->get($question->id);
    }

    /**
     * @return Question
     */
    public function getNextQuestion()
    {
        $firstUnansweredQuestionId = $this->answers->search(function (Collection $questionAnswers) {
            return $questionAnswers->isEmpty();
        });

        return $this->findQuestionById($firstUnansweredQuestionId);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectToNextQuestion()
    {
        return redirect()->route('survey.show', $this->getNextQuestion()->id);
    }

    public function getQuestionBefore(Question $question)
    {
        return $this->questions->get($this->getQuestionOffset($question) - 1);
    }

    public function getQuestionAfter(Question $question)
    {
        return $this->questions->get($this->getQuestionOffset($question) + 1);
    }

    /**
     * @param \App\Models\Question $question
     * @return bool
     */
    public function questionIsFirst(Question $question)
    {
        return $this->questions->first()->id === $question->id;
    }

    /**
     * @param \App\Models\Question $question
     * @return bool
     */
    public function questionIsLast(Question $question)
    {
        return $this->questions->last()->id === $question->id;
    }

    /**
     * @param \App\Models\Question $question
     * @return int
     */
    public function questionOrder(Question $question)
    {
        return $this->getQuestionOffset($question) + 1;
    }

    /**
     * @return int
     */
    public function totalQuestions()
    {
        return $this->questions->count();
    }

    /**
     * @param \App\Models\Question $question
     * @return bool
     */
    public function questionCanBeAnswered(Question $question)
    {
        if (!$nextQuestion = $this->getNextQuestion()) {
            return true;
        }

        $requestedOffset = $this->getQuestionOffset($question);
        $nextQuestion = $this->getQuestionOffset($nextQuestion);

        return $requestedOffset <= $nextQuestion;
    }

    /**
     * @param int $questionId
     * @param array $answer
     * @return $this
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addAnswerToQuestion(Question $question, array $answers)
    {
        if (!$this->questionCanBeAnswered($question)) {
            throw new \InvalidArgumentException("The provided question ({$question->text}) cannot be answered yet.");
        }

        $this->answers->put($question->id, collect($answers));

        return $this;
    }

    /**
     * @return $this
     * @throws \Throwable
     */
    public function complete()
    {
        $answerSheet = $this->createAnswerSheet();

        $answers = $this->processAnswers($answerSheet);

        DB::transaction(function () use ($answers) {
            $answers->chunk(100)->each(function (Collection $chunkedAnswers) {
                DB::table('answers')->insert($chunkedAnswers->toArray());
            });
        });

        $this->session->put(self::KEY_STATUS, self::STATUS_COMPLETED);

        return $this;
    }

    /**
     * @return mixed
     */
    protected function prepareQuestions()
    {
        return Question::all();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    protected function prepareAnswers()
    {
        return $this->questions->mapWithKeys(function (Question $question) {
            return [$question->id => collect()];
        });
    }

    /**
     * @return void
     */
    protected function persist()
    {
        $this->session->put(self::KEY_QUESTIONS, $this->questions);
        $this->session->put(self::KEY_ANSWERS, $this->answers);
    }

    /**
     * @param string $questionId
     * @return Question|null
     */
    protected function findQuestionById(string $questionId)
    {
        return $this->questions->first(function (Question $question) use ($questionId) {
            return $question->id == $questionId;
        });
    }

    /**
     * @param \App\Models\Question $question
     * @return int|null;
     */
    protected function getQuestionOffset(Question $question)
    {
        return $this->questions->search(function (Question $searchedQuestion) use ($question) {
            return $question->id == $searchedQuestion->id;
        });
    }

    /**
     * @return AnswerSheet
     */
    protected function createAnswerSheet()
    {
        $answerSheet = new AnswerSheet();
        $answerSheet->department_id = $this->code->department_id;
        $answerSheet->semester = $this->code->semester;

        return tap($answerSheet)->save();
    }

    /**
     * @param \App\Models\AnswerSheet $answerSheet
     * @return \Illuminate\Support\Collection
     */
    protected function processAnswers(AnswerSheet $answerSheet)
    {
        return $this->answers->map(function (Collection $questionAnswers) use ($answerSheet) {
            return $questionAnswers->map(function (array $answer) use ($answerSheet) {
                $answer['answer_sheet_id'] = $answerSheet->id;

                return $answer;
            })->toArray();
        })->flatten(1);
    }

    protected function markCodeUsed()
    {
        $this->code->used = true;

        $this->code->update();
    }
}
