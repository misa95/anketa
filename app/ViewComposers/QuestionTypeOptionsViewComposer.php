<?php

namespace App\ViewComposers;

use App\Models\Question;
use Illuminate\View\View;

class QuestionTypeOptionsViewComposer
{
    public function compose(View $view)
    {
        $view->with('typeOptions', Question::TYPE_OPTIONS);
    }
}