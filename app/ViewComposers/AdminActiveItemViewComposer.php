<?php

namespace App\ViewComposers;

use Illuminate\View\View;

class AdminActiveItemViewComposer
{
    public function compose(View $view)
    {
        $view->with('activeItem', request()->route()->action['active_item'] ?? null);
    }
}