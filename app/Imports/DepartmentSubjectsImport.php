<?php

namespace App\Imports;

use App\Models\Department;
use App\Models\Teacher;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DepartmentSubjectsImport implements ToArray, WithHeadingRow
{
    const KEY_DEPARTMENT = 'smer';
    const KEY_SEMESTER   = 'semestar';
    const KEY_SUBJECT    = 'predmet';
    const KEY_TEACHER    = 'profesor';
    const KEY_ASSISTANT  = 'asistent';

    const MANDATORY_KEYS = [
        self::KEY_SEMESTER,
        self::KEY_SUBJECT,
        self::KEY_TEACHER,
    ];

    /** @var \App\Models\Department  */
    private $department;

    /** @var \Illuminate\Support\Collection  */
    private $teachers;

    /** @var array */
    public $subjects = [];

    public function __construct(Department $department, Collection $teachers)
    {
        $this->department = $department;
        $this->teachers   = $this->prepareTeachers($teachers);
    }

    public static function forDepartment(Department $department)
    {
        return new static($department, Teacher::all());
    }

    /**
     * @param array $row
     * @return array
     */
    public function array(array $rows)
    {
        foreach ($rows as $row) {
            try {
                $this->validateRow($row);
            } catch (\InvalidArgumentException $e) {
                continue;
            }

            $this->subjects[] = $this->convertRowToSubjectData($row);
        }
    }

    protected function convertRowToSubjectData(array $row)
    {
        $teacherId = $this->findOrCreateTeacher($row[self::KEY_TEACHER])->id;
        $assistantId = ($this->rowHasAssistant($row))
            ? $this->findOrCreateTeacher($row[self::KEY_ASSISTANT])->id
            : null;

        return [
            'name'          => $row[self::KEY_SUBJECT],
            'semester'      => $row[self::KEY_SEMESTER],
            'teacher_id'    => $teacherId,
            'assistant_id'  => $assistantId,
            'department_id' => $this->department->id,
        ];
    }

    /**
     * @param array $row
     */
    protected function validateRow(array $row)
    {
        foreach (self::MANDATORY_KEYS as $key) {
            if (empty($row[$key])) {
                throw new \InvalidArgumentException(sprintf(
                    "The provided row is missing the '%s' key", $key
                ));
            }
        }
    }

    /**
     * @param array $row
     * @return bool
     */
    protected function rowHasAssistant(array $row)
    {
        return !empty($row[self::KEY_ASSISTANT]);
    }

    /**
     * @param string $teacherName
     * @return \App\Models\Teacher
     */
    protected function findOrCreateTeacher(string $teacherName)
    {
        $teacherName = trim($teacherName);

        return $this->teachers->get($teacherName) ?? $this->createTeacher($teacherName);
    }

    /**
     * @param \Illuminate\Support\Collection $teachers
     * @return Collection
     */
    protected function prepareTeachers(Collection $teachers)
    {
        return $teachers->keyBy->name;
    }

    /**
     * @param string $name
     * @return \App\Models\Teacher
     */
    protected function createTeacher(string $name)
    {
        $teacher = Teacher::create(['name' => $name]);

        $this->teachers->put($name, $teacher);

        return $teacher;
    }
}
