<?php

namespace App\Console\Commands;

use App\Models\Settings;
use Illuminate\Console\Command;

class InitializeSettings extends Command
{
    protected $signature = 'settings:initialize';

    protected $description = 'Initializes default values for settings';

    public function handle()
    {
        Settings::set('codes-public', false);
    }
}
