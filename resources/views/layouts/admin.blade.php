<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@section('title') {{ config('app.name') }} @show - Админ | Анкета </title>

    <!-- Scripts -->
    @section('scripts')
        <script src="{{ asset('js/app.js') }}"></script>
    @show

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ route('admin.departments.index') }}">
                Анкета
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                @auth('admin')
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link @if($activeItem == 'teachers') active @endif" href="{{ route('admin.teachers.index') }}">Професори</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if($activeItem == 'questions') active @endif" href="{{ route('admin.questions.index') }}">Питања</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if($activeItem == 'departments') active @endif" href="{{ route('admin.departments.index') }}">Предмети</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if($activeItem == 'codes') active @endif" href="{{ route('admin.departments.codes.first') }}">Кодови</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if($activeItem == 'reports') active @endif" href="{{ route('admin.reports.index') }}">Извештаји</a>
                        </li>
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->email }} <span class="caret"></span>
                        </a>
                        <li class="nav-item dropdown">
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('admin.logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Одјави се
                                </a>

                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                @endauth
            </div>
        </div>
    </nav>

    @section('flash')
        <div class="container">
            @include('layouts.flash')
        </div>
    @show

    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>
</html>
