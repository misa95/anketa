<div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <select class="form-control" id="{{ $name }}" name="{{ $name }}">
        @if ($placeholder ?? false)
            <option value="" disabled selected>{{ $placeholder }}</option>
        @endif
        @foreach ($options as $value => $option)
            <option value="{{ $value }}" @if((old($name, $selected ?? false) == $value)) selected @endif>{{ $option }}</option>
        @endforeach
    </select>
    @if ($errors->has($name)) <label class="error-message" for="{{ $name }}">{{ $errors->first($name) }}</label> @endif
</div>