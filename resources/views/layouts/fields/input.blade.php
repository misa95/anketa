<div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <input type="{{ $type ?? 'text' }}"
           name="{{ $name }}"
           class="form-control"
           id="{{ $name }}"
           value="{{ old($name, $value ?? '')}}"
           @if($disabled ?? false) disabled @endif
           @if ($autofocus ?? false) autofocus @endif
    >
    @if ($errors->has($name)) <label class="error-message" for="{{ $name }}">{{ $errors->first($name) }}</label> @endif
</div>
