<div class="modal fade" id="{{ $name }}" tabindex="-1" role="dialog" aria-labelledby="{{ $name }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="{{ $name }}">{{ $title }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($warning ?? false)
                    <div class="d-flex text-danger">
                        {{ $warning }}
                    </div>
                @endif
                <div class="d-flex">
                    {{ $text }}
                </div>
                <div class="d-flex">
                    <input type="password" class="form-control password-confirm" name="{{ $passwordInput ?? 'password' }}">
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-success" value="{{ $action }}">
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{ $cancel }}</button>
            </div>
        </div>
    </div>
</div>
