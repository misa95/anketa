<div class="modal fade" id="{{ $name ?? 'loading-modal' }}" tabindex="-1" role="dialog" aria-labelledby="{{ $name ?? 'loading-modal' }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="{{ $name ?? 'loading-modal' }}">{{ $title ?? 'Учитавање' }}</h5>
            </div>
            <div class="modal-body d-flex justify-content-center">
                <div class="spinner-border" role="status">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
    </div>
</div>
