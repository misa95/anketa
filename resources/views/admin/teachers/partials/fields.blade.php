@include('layouts.fields.input', [
    'name'      => 'name',
    'label'     => 'Име професора',
    'value'     => $teacher->name,
    'autofocus' => true,
])
