@extends('layouts.admin')

@section('title', 'Професори')

@section('scripts')
    @parent

    <script>
        $(document).ready(function(){
            $("#search-input").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#teachers-table tr").not('thead tr').filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection


@section('content')
    <div class="container">
        <div class="col-md-10">
            <div class="d-flex justify-content-end mb-2">
                <a class="btn btn-secondary" href="{{ route('admin.teachers.create') }}">Додај професора</a>
            </div>

            <div class="d-flex justify-content-end mb-2">
                <input class="form-control" id="search-input" type="text" placeholder="Претражи професоре">
            </div>

            <table class="table" id="teachers-table">
                <thead class="thead-dark">
                <th scope="col">
                    Професор
                </th>
                <th scope="col">
                    Опције
                </th>
                </thead>
                <tbody>
                @foreach ($teachers as $teacher)
                        <tr>
                            <th class="align-middle" scope="row">
                                <a href="{{ route('admin.teachers.show', $teacher->id) }}">
                                    {{ $teacher->name }}
                                </a>
                            </th>
                            <td>
                                <div class="row">
                                    <div class="mr-1">
                                        @include('admin.partials.buttons.delete', [
                                            'url' => route('admin.teachers.destroy', $teacher->id),
                                            'modalId' => "delete-{$teacher->id}",
                                            'modalTitle' => "Избриши {$teacher->name}",
                                            'modalText' => "Потврди брисање професора {$teacher->name}",
                                        ])
                                    </div>
                                    @include('admin.partials.buttons.edit', ['url' => route('admin.teachers.edit', $teacher->id)])
                                </div>
                            </td>
                        </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection
