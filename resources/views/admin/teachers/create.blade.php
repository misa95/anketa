@extends('layouts.admin')

@section('title', 'Додај професора')

@section('content')
    <div class="container">
        <div class="h1 text-center">
            Додај професора
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <form method="POST" action="{{ route('admin.teachers.store') }}">
                    @csrf

                    @include('admin.teachers.partials.fields')

                    <div class="d-flex justify-content-center">
                        @include('admin.partials.form_actions', [
                            'submitCta' => 'Сачувај',
                            'cancelUrl' => route('admin.teachers.index'),
                            'cancelCta' => 'Откажи',
                        ])
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
