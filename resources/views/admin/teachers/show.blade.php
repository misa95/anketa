@extends('layouts.admin')

@section('title', $teacher->name)

@section('content')
    <div class="container">
        <div class="col-md-10">
            <div class="h1 text-center">
                {{ $teacher->name }}
            </div>

            <div class="d-flex justify-content-end mb-3">
                <a class="btn btn-secondary px-3 mr-1" href="{{ route('admin.teachers.edit', $teacher->id) }}">Измени</a>
            </div>

            <div class="h2 text-center">
                Предмети
            </div>

            <table class="table">
                <thead class="thead-dark">
                    <th scope="col">
                        Предмет
                    </th>
                    <th scope="col">
                        Смер
                    </th>
                    <th scope="col">
                        Улога
                    </th>
                </thead>
                <tbody>
                    @foreach($subjects as $subject)
                        <tr>
                            <th>
                                {{ $subject->name }}
                            </th>
                            <td>
                                {{ $subject->department->name }}
                            </td>
                            <td>
                                {{ $subject->role }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
