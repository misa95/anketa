<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./snow.css" stylesheet="text/css">
    <title>Document</title>
    <style>
        .wrap{
            overflow: visible;
            min-width: 595px;
            /*height: 842px;*/
            display: flex;
            flex-direction: column;
            margin: auto;
        }
        .header{
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-content: space-between;
            margin-top: 150px;
        }
        .header .text{
            width: 300px;
            margin-top: 20px;
            text-align: center;
            padding-right: 30px;
        }
        .header .logo{
            padding-right: 50px;
        }
        .mini-table-wrap{
            margin-top: 50px;
            margin-bottom: 30px;
        }
        .mini-table-wrap .mini-table{
            border: 1px solid black;
        }
        .mini-table tr td{
            border: 1px solid black;
            font-size: 1.5em;
            font-weight: bold;
            text-align: center;
        }
        .paragraph-text{

            font-size: 1.1em;
            /*text-align: left;*/
        }
        .tabela{
            table-layout: fixed;
            border-collapse: collapse;
            page-break-inside: always;
        }
        .tabela tr th{
            border: 1px solid black;
            text-align: center;
            padding: 4px 4px;
            font-size: 0.9em;



        }
        .tabela tr td{
            padding: 4px 4px;
            border: 1px solid black;
            text-align: center;
            font-weight: bold;
            font-size: 1.1em;


            /*display: block;*/

        }
        .tabela .last{
            font-size: 1.2em;
            background-color: #a39b60;
        }
        .small tr td{
            /*display: block;*/
            font-size: 1em;
            padding: 5px 10px;
            text-align: center;


        }
        .small tr td:first-child{
            text-align: left;
            padding-left: 10px;
        }

        .header-2{
            text-align: center;
            font-size: 1.4em;
            color: #fff;
            width: 400px;
            background-color: #0f64c0;
            border: 1px solid black;
            margin: 0 auto;
            padding: 5px auto;

        }
        .mini-head{
            text-decoration: underline;
            font-size: 1.2em;
        }
        td, th, tr {
            overflow: visible;
            page-break-inside: avoid;
        }
        /*@media print {
          table { page-break-after:auto }
          tr    { page-break-inside:avoid; page-break-after:auto }
          td    { page-break-inside:avoid; page-break-after:auto }
        }*/

    </style>
</head>
<body>
<div class="wrap">
    <div class="header">
        <div class="text"><b>ВИСОКА ТЕХНИЧКА ШКОЛА СТРУКОВНИХ СТУДИЈА У НИШУ</b></div>
    <!-- <img src="{{ asset('public/img/vtsLogo.jpg') }}"> --> <!-- NE RADI! -->
    </div>
    <div class="mini-table-wrap">
        <table class="mini-table" style="page-break-inside:avoid;">
            <tr>
                <td>Извештај Комисије за обезбеђење квалитета о студентском вредновању педагошког рада наставника и сарадника, квалитета наставних предмета и средстава и рада стручних служби у зимском семестру шк. {{ $schoolYear }}. год.</td>
            </tr>
        </table>
    </div>
    <div class="paragraph-text" style="text-indent: 50px;">
        У поступку анкетирања учествовало је <b>{{ $surveyedCount }}</b> од укупно <b>{{ $studentTotal }}</b> првоуписаних студената <b>({{ $surveyedPerc }}%)</b> са свих шест акредитованих студијских програма основних струковних студија, на све три године студија. Врeднован је педагошки рад наставника и сарадника на предметима зимског семестра школске {{ $schoolYear }}, квалитет самих предмета и наставних средстава, као и рад стручних служби и хигијене у Школи.

    </div>
    <div class="paragraph-text" style="text-indent: 50px;">
        Поступак анкетирања је спровела Студентска служба, на јединствен начин за све студенте, током овере семестра. У поступку припреме материјала, начина и рокова спровођења анкетирања, Студентска служба је имала стручну помоћ од стране Комисије за обезбеђење квалитета. Комисија је извршила обраду анкетних листића, а на основу резултата анкетирања је израдила и овај извештај, по студијским програмима и на нивоу целе Школе.
    </div>
    <br>
    <br>
    <div class="paragraph-text">
        <b>Напомена</b>: Комисија је списак предмета и наставника који изводе наставу на тим предметима, добила од студентске службе (којој су шефови студијских програма потврдили исправност списка).
    </div>
    <br>
    <div class="paragraph-text">
        Табеларни приказ oцена педагошког рада наставника и сарадника:
    </div>
    <br>
    <table class="tabela" style="page-break-inside:avoid;">

        <tr>
            <th><i>ВТШ НИШ</i></th>
            <th>Број анкетираних студената / број првоуписаних</th>
            <th>Проценат анкетираних студената</th>
            <th>Укупно оцена</th>
            <th>Просечна оцена</th>
        </tr>

        <tbody style="page-break-inside:avoid;">
        @foreach ($departments as $department)
            <tr>
                <td>{{ $department->get('name') }}</td>
                <td>{{ $department->get('surveyed') }}/{{ $department->get('total') }}</td>
                <td>{{ $department->get('percentage') }}%</td>
                <td>{{ $department->get('markCount') }}</td>
                <td>{{ $department->get('averageMark') }}</td>
            </tr>
        @endforeach
        <tr class="last">
            <td>ВТШ укупно</td>
            <td>{{ $surveyedCount }}/{{ $studentTotal }}</td>
            <td>{{ $surveyedPerc }}%</td>
            <td>137</td>
            <td>{{ $departmentAverage }}</td>
        </tr>
        </tbody>
    </table>
    <br>
    <div class="paragraph-text">
        <b>Просечна оцена свих наставника и сарадника је {{ $teacherAverage->get('numeric') }} ({{ $teacherAverage->get('word') }})</b>
    </div>
    <br>
    <br>
    <div class="paragraph-text">
        Укупно су студенти на свим студијским програмима основних студија, у зимском семестру, слушали наставу из {{ $subjectCount }} предмета. Студенти су оценили квалитет свих предмета, просечном оценом <b>{{ $subjectOverview->get('numeric') }} ({{ $subjectOverview->get('word') }})</b>. По студијсим програмима то изгледа:
    </div>
    <br>
    <table class="tabela" style="page-break-inside:avoid;">

        <tr >
            <th colspan="3" style="font-weight: 700; font-size: 1.3em;">КВАЛИТЕТ ПРЕДМЕТА</th>
        </tr>
        <tr>
            <th>Студијски програм</th>
            <th>Број оцењиваних предмета</th>
            <th>Просечна оцена</th>
        </tr>

        <tbody style="page-break-inside:avoid;">
        @foreach ($departments as $department)
            <tr>
                <td>{{ $department->get('name') }}</td>
                <td>{{ $department->get('subjectCount') }}</td>
                <td>{{ $department->get('subjectAverage') }}</td>
            </tr>
        @endforeach
        <tr style="background-color: #d3d19d;">
            <td>ВТШ</td>
            <td>{{ $subjectCount }}</td>
            <td>{{ $subjectOverview->get('numeric') }}</td>
        </tr>
        </tbody>
    </table>
    <br>
    <div class="paragraph-text">
        Студенти су врло повољно оценили квалитет наставних средстава, рад стручних служби и хигијену у Школи.
    </div>
    <br>
    <table class="tabela" style="page-break-inside:avoid;">
        <tr>
            <th>Студијски програм</th>
            @foreach ($otherQuestions as $question)
                <th>{{ $question->text }}</th>
            @endforeach

        </tr>
        <tbody style="page-break-inside:avoid;">
        @foreach ($departments as $department)
            <tr>
                <td>{{ $department->get('name') }}</td>
                @foreach($otherQuestions as $question)
                    <td>{{ $department->get('otherAnswers')->get($question->id) }}</td>
            @endforeach
            </tr>
        @endforeach
        <tr style="background-color: #d3d19d;">
            <td>ВТШ</td>
            @foreach ($otherQuestions as $question)
                <td>{{ $otherAnswers->get($question->id) }}</td>
            @endforeach
        </tr>
        </tbody>
    </table>
    <br>
    <br>
    <div class="paragraph-text">
        <b>Напомена</b> Комисија је успоставила следеће критеријуме за оцену наставника: Одличан (4.5 до 5), Врлодобар (4 до 4.5), Добар (3.5 до 4),  Довољан (3 до 3.5) и Недовољан (испод 3). У даљем тексту дата је анализа по студијским програмима.
    </div>
    <br>

    {{-- TODO: Ovde krecu smerovi --}}
    @foreach ($departments as $department)
        <div class="header-2">
        {{ $department->get('name') }}
        </div>
        <br>
        <div class="paragraph-text">
            У анкети, на овом студијском програму, учествовало је {{ $department->get('surveyed') }} студената од {{ $department->get('total') }} првоуписана на овај студијски програм ({{ $department->get('percentage') }}%).
            Оцењиван је рад укупно наставника {{ $department->get('teacherCount') }} ({{ $department->get('markCount') }} оценa), на све три године студија. Просечна оцена свих наставника, на овом студијском програму је <b>{{ $department->get('teacherAverage')->get('numeric') }} ({{ $department->get('teacherAverage')->get('word') }})</b>.
        </div>
        <br>
        <table class="tabela" style="page-break-inside:avoid;">

            <tr>
                <th style="background-color: #0f64c0; color: #fff;">Студијски програм</th>
                <th>Број анкетираних /уписаних студената</th>
                <th>Проценат анкетираних студената</th>
                <th>Број оцењиваних наставника/ Укупно оцена</th>
                <th>Просечна оцена</th>
            </tr>

            <tbody style="page-break-inside:avoid;">
            <tr>
                <td style="background-color: #0f64c0; color: #fff;">{{ $department->get('shorthand') }}</td>
                <td>{{ $department->get('surveyed') }}/{{ $department->get('total') }}</td>
                <td>{{-- $department['overview']['percentage'] --}}10%</td>
                <td>{{ $department->get('teacherCount') }}/{{ $department->get('markCount') }}</td>
                <td>{{ $department->get('average') }}</td>
            </tr>
            </tbody>
        </table>
        <br>
        <div class="paragraph-text">
            Укупно су студенти, на све три године студија, слушали наставу из {{ $department->get('subjectCount') }} предмета и оценили квалитет предмета оценом.
        </div>
        <br>
        <table class="tabela" style="page-break-inside:avoid;">

            <tr>
                <th>Студијски програм</th>
                <th>Број оцењиваних предмета</th>
                <th>Просечна оцена</th>
            </tr>

            <tbody style="page-break-inside:avoid;">
            <tr>
                <td>{{ $department->get('shorthand') }}</td>
                <td>{{ $department->get('subjectCount') }}</td>
                <td>{{ $department->get('subjectAverage') }}</td>
            </tr>
            </tbody>
        </table>
        <br>
        <div class="paragraph-text">
            Оцене свих оцењиваних аспеката, по годинама студија, дате су у наредним табелама.
        </div>
        <br>
        @foreach ($department->get('years') as $year)
            <div class="mini-head">
                <b>{{ $year->get('year') }}. ГОДИНА </b>
            </div>
            <div class="paragraph-text">
                На <b>{{ $year->get('year') }}. години студија</b>, у анкети је учествовало {{ $year->get('surveyed') }} студената. Студенти су оцењивали педагошки рад {{ $year->get('teacherCount') }} наставника и сарадника ({{ $year->get('markCount') }} оцена) из {{ $year->get('subjectCount') }} предмета. Просечна оцена свих наставника {{ $year->get('year') }}. године студија је <b>{{ $year->get('teacherAverage')->get('numeric') }} ({{ $year->get('teacherAverage')->get('word') }}).</b>
            </div>
            <br>
            <table class="tabela small" style="page-break-inside:avoid;">

                <tr style="font-size: 1.3em; padding: 5px 10px;">
                    <th colspan="4">{{ $department->get('shorthand') }}  {{ $year->get('year') }}. година</th>
                </tr>

                <tbody style="page-break-inside:avoid;">
                @foreach($year->get('subjects') as $subject)
                    <tr>
                        <td>{{ $subject->get('name') }}</td>
                        <td>Н</td>
                        <td>{{ $subject->get('teacher') }}</td>
                        <td>{{ $subject->get('teacherAverage') }}</td>
                    </tr>
                    @if ($subject->get('assistant'))
                        <tr>
                            <td>{{ $subject->get('name') }}</td>
                            <td>С</td>
                            <td>{{ $subject->get('assistant') }}</td>
                            <td>{{ $subject->get('assistantAverage') }}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
            <br>

            <div class="paragraph-text">
                Оцењиван је и квалитет и организација предмета, на којима су наставници изводили наставу.
            </div>
            <br>
            <table class="tabela small" style="page-break-inside:avoid;">

                <tr>
                    <th colspan="{{ $year->get('subjectCount') }}" style="font-size: 1.2em;">{{ $department->get('shorthand') }} {{ $year->get('year') }}. година</th>
                </tr>

                <tbody style="page-break-inside:avoid;">
                <tr>
                    @foreach ($year->get('subjects') as $subject)
                        <td>{{ $subject->get('name') }}</td>
                    @endforeach
                </tr>
                <tr>
                    @foreach($year->get('subjects') as $subject))
                        <td>{{ $subject->get('average') }}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
            <br>
            <div class="paragraph-text">
                На крају су студенти оцењивали квалитет одговарајућих служби Школе и квалитет услова рада, те наставних средстава.
            </div>
            <br>
            <br>
            <table class="tabela small" style="page-break-inside:avoid;">

                <tr>
                    <th colspan="{{ $year->get('otherAnswers')->count() }}" style="font-size: 1.2em;">{{ $department->get('shorthand') }}  {{ $year->get('year') }}. година</th>
                </tr>

                <tbody style="page-break-inside:avoid;">
                <tr>
                    @foreach($otherQuestions as $question)
                        <td>{{ $question->text }}</td>
                    @endforeach
                </tr>
                <tr>
                    @foreach($otherQuestions as $question)
                        <td>{{ $year->get('otherAnswers')->get($question->id) }}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
            <br>
        @endforeach

    @endforeach
</div>
</body>
</html>
