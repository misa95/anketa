@extends('layouts.admin')

@section('title', 'Кодови')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-2">
                @include('admin.departments.navigation', ['departments' => $departments, 'activeDepartment' => $department->id, 'routeLink' => 'admin.departments.codes.index'])
            </div>

            <div class="col-md-10">
                <div class="d-flex justify-content-between mb-3">
                    <div>
                        Број неискоришћених кодова за смер: {{ $totalAvailableCodes }}
                        <br>
                        Број искоришћених кодова за смер: {{ $totalUsedCodes }}
                    </div>
                    <div class="d-flex  justify-content-end">
                        <form>
                            <a class="btn btn-secondary mr-1" href="{{ route('admin.departments.codes.create', $department->id) }}">Генериши кодове</a>
                        </form>

                        @if(Settings::get('codes-public'))
                            <form action="{{ route('admin.codes.hide') }}" method="post">
                                @csrf

                                <button type="submit" class="btn btn-secondary">Сакриј кодове</button>
                            </form>
                        @else
                            <form action="{{ route('admin.codes.publish') }}" method="post">
                                @csrf

                                <button type="submit" class="btn btn-secondary">Објави кодове</button>
                            </form>
                        @endif
                    </div>
                </div>

                <ul class="nav nav-tabs" id="semesters" role="tablist">
                    @foreach ($codes as $semester => $semesterCodes)
                        <li class="nav-item">
                            <a
                                    class="nav-link @if($loop->first) active @endif "
                                    id="{{ $semester }}-tab"
                                    data-toggle="tab"
                                    href="#semester-{{ $semester }}"
                                    role="tab"
                                    aria-controls="semester-{{ $semester }}"
                                    @if ($loop->first)
                                    aria-selected="true"
                                    @endif
                            >{{ $semester }}. семестар</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content mt-4">
                    @foreach ($codes as $semester => $semesterCodes)
                        <div class="tab-pane @if($loop->first) active @endif" id="semester-{{ $semester }}" role="tabpanel" aria-labelledby="{{ $semester }}-tab">
                            <div class="d-flex justify-content-between mb-2">
                                <div>
                                    Број неискоришћених кодова за {{ $semester }}. семестар: {{ $semesterCodes->where('used', false)->count() }}
                                    <br>
                                    Број искоришћених кодова за {{ $semester }}. семестар: {{ $semesterCodes->where('used', true)->count() }}
                                </div>
                                <a
                                    href="{{ route('admin.codes.print', ['department' => $department, 'semester' => $semester]) }}"
                                    target="_blank"
                                    class="btn btn-secondary">
                                    Одштампај кодове за {{ $semester }}. семестар
                                </a>
                            </div>
                            <table class="table">
                                <thead class="thead-dark">
                                    <th scope="col">
                                        Код
                                    </th>
                                    <th scope="col">
                                        Смер
                                    </th>
                                    <th scope="col">
                                        Семестар
                                    </th>
                                </thead>
                                <tbody>
                                    @foreach ($semesterCodes as $code)
                                        <tr>
                                            <th class="align-middle" scope="row">{{ $code->code }}</th>
                                            <td class="align-middle">{{ $department->name }}</td>
                                            <td class="align-middle">{{ $semester  }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                </div>

                @if ($codes->isEmpty())
                    <div class="h2 text-center">
                        Нема неупотребљених кодова
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
