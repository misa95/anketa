@extends('layouts.admin')

@section('title', 'Генериши кодове')

@section('content')
    <div class="container">
        <div class="h1 text-center">
            Генериши кодове
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <form method="POST"action="{{ route('admin.departments.codes.store', $department->id) }}">
                    @csrf

                    @include('admin.codes.partials.fields')

                    <div class="d-flex justify-content-center">
                        @include('admin.partials.form_actions', [
                            'submitCta' => 'Генериши',
                            'cancelUrl' => route('admin.departments.codes.index', $department->id),
                            'cancelCta' => 'Откажи',
                        ])
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
