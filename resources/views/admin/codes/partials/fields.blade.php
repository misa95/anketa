@include('layouts.fields.input', [
    'name'     => 'department',
    'label'    => 'Смер',
    'value'    => $department->name,
    'disabled' => true,
])

@include('layouts.fields.input', [
    'name'      => 'semester',
    'label'     => 'Семестар',
    'type'      => 'number',
    'autofocus' => true,
])

@include('layouts.fields.input', [
    'name'  => 'batch',
    'label' => 'Количина',
    'type'  => 'number',
])
