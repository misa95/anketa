@foreach($codes->chunk(100) as $pageCodes)
    <table style="width: 95%; margin: auto; border-collapse: collapse;" border="1px solid black">
        <thead>
            <tr>
                <th colspan="4" style="font-size: 40px">
                    {{ $department->shorthand }} - {{ $semester }}. семестар
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($pageCodes->chunk(4) as $rowCodes)
                <tr>
                    @for($i = 0; $i < 4; $i++)
                        <td align="center" style="font-size: 23px">
                            {{ optional($rowCodes->values()->get($i))->code }}
                        </td>
                    @endfor
                </tr>
            @endforeach
        </tbody>
    </table>
    <div style="page-break-after: always"></div>
@endforeach
<script>
    window.print();
    window.onfocus = () => { window.close(); }
</script>
