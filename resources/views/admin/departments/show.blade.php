@extends('layouts.admin')

@section('title', $department->name)

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                @include('admin.departments.navigation', ['departments' => $departments, 'activeDepartment' => $department->id, 'routeLink' => 'admin.departments.show'])
            </div>

            <div class="col-md-10">
                <div class="d-flex justify-content-end mb-3">
                    <a class="btn btn-secondary mr-1" href="{{ route('admin.departments.subjects.create', $department->id) }}">Додај предмет</a>
                    <button class="btn btn-secondary" id="upload-excel-button" type="button">Учитај Ексел</button>
                    <form method="POST" action="{{ route('admin.departments.import', $department->id) }}" id="upload-excel-form" enctype="multipart/form-data">
                        @csrf
                        <input type="file" id="upload-excel-input" name="excel" hidden>
                    </form>
                    <a class="btn btn-secondary ml-1" href="{{ route('admin.departments.edit', $department->id) }}">Измени смер</a>
                </div>
                <div class="d-flex justify-content-end mb-3">
                    <a href="{{ asset('storage/Excel primer.xlsx') }}"><i style="color: green" class="fa fa-file-excel-o"></i>Пример Ексела</a>
                </div>
                <ul class="nav nav-tabs" id="semesters" role="tablist">
                    @foreach ($semesters as $semester => $subjects)
                        <li class="nav-item">
                            <a
                                    class="nav-link @if($loop->first) active @endif "
                                    id="{{ $semester }}-tab"
                                    data-toggle="tab"
                                    href="#semester-{{ $semester }}"
                                    role="tab"
                                    aria-controls="semester-{{ $semester }}"
                                    @if ($loop->first)
                                        aria-selected="true"
                                    @endif
                            >{{ $semester }}. семестар</a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content mt-4">
                    @foreach ($semesters as $semester => $subjects)
                        <div class="tab-pane @if($loop->first) active @endif" id="semester-{{ $semester }}" role="tabpanel" aria-labelledby="{{ $semester }}-tab">
                            <table class="table">
                                <thead class="thead-dark">
                                    <th scope="col">
                                        Предмет
                                    </th>
                                    <th scope="col">
                                        Професор
                                    </th>
                                    <th scope="col">
                                        Асистент
                                    </th>
                                    <th scope="col">
                                        Семестар
                                    </th>
                                    <th scope="col">
                                        Опције
                                    </th>
                                </thead>
                                <tbody>
                                    @foreach ($subjects->sortBy('name') as $subject)
                                        <tr>
                                            <th class="align-middle" scope="row">{{ $subject->name }}</th>
                                            <td class="align-middle">{{ $subject->teacher->name }}</td>
                                            <td class="align-middle">{{ optional($subject->assistant)->name }}</td>
                                            <td class="align-middle">{{ $subject->semester }}</td>
                                            <td>
                                                <div class="row">
                                                    <div class="mr-1">
                                                        @include('admin.partials.buttons.delete', [
                                                            'url' => route('admin.subjects.destroy', $subject->id),
                                                            'modalId' => "delete-{$subject->id}",
                                                            'modalTitle' => "Избриши {$subject->name}",
                                                            'modalText' => "Потврди брисање {$subject->name}",
                                                        ])
                                                    </div>
                                                    @include('admin.partials.buttons.edit', ['url' => route('admin.subjects.edit', $subject->id)])
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function () {
            $('#upload-excel-button').on('click', function () {
                $('#upload-excel-input').trigger('click');
            });
            $('#upload-excel-input').on('change', function () {
                $('#upload-excel-form').submit();
            });
        });
    </script>
@endsection
