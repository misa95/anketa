<div class="list-group">
    @foreach ($departments as $department)
        <a href="{{ route($routeLink, $department->id) }}" class="list-group-item list-group-item-action @if($department->id == $activeDepartment) active @endif ">
            {{ $department->name }}
        </a>
    @endforeach
    <a href="{{ route('admin.departments.create') }}" class="list-group-item list-group-item-action"><i class="fa fa-plus-circle mr-2"></i>Додај смер</a>
</div>
