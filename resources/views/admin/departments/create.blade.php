@extends('layouts.admin')

@section('title', "Додај смер")

@section('content')
    <div class="container">
        <div class="h1 text-center">
            Додај смер
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <form method="POST" action="{{ route('admin.departments.store') }}">
                    @csrf

                    @include('admin.departments.partials.fields')

                    <div class="d-flex justify-content-center">
                        @include('admin.partials.form_actions', [
                            'submitCta' => 'Сачувај',
                            'cancelUrl' => route('admin.departments.index'),
                            'cancelCta' => 'Откажи',
                        ])
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
