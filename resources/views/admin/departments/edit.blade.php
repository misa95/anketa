@extends('layouts.admin')

@section('title', "Измени {$department->name}")

@section('content')
    <div class="container">
        <div class="h1 text-center">
            {{ $department->name}}
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <form method="POST" action="{{ route('admin.departments.update', $department->id) }}">
                    @csrf
                    @method('PATCH')

                    @include('admin.departments.partials.fields')

                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-success mr-3">Сачувај</button>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-modal">Избриши</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <form action="{{ route('admin.departments.destroy', $department) }}" method="POST">
        @csrf
        @method('DELETE')

        @include('layouts.confirm_modal', [
            'name' => 'delete-modal',
            'title' => "Избриши {$department->name}",
            'text' => "Потврди брисање смера {$department->name}",
            'action' => 'Избриши',
            'cancel' => 'Одустани',
        ])
    </form>
@endsection
