@include('layouts.fields.input', [
    'name'     => 'name',
    'label'    => 'Име',
    'value'    => $department->name ?? null,
    'autofocus' => true,
])

@include('layouts.fields.input', [
    'name' => 'shorthand',
    'label' => 'Акроним',
    'value' => $department->shorthand ?? null
])

@include('layouts.fields.input', [
    'name'  => 'total_students',
    'label' => 'Број студената',
    'value' => $department->total_students ?? null,
])
