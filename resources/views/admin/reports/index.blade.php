@extends('layouts.admin')

@section('title', 'Извештаји')

@section('scripts')
    @parent
    <script>
        $(document).ready(function () {
            var form = $('#archive-form');

            form.submit(function (e) {
                $('#loading-modal').modal('show');
            });
        });
    </script>
@endsection

@section('content')
    @include('layouts.loading-modal', [
        'title' => 'Генерисање извештаја'
    ]);
    <div class="container">
        <div class="d-flex justify-content-end mb-3">
            <form method="POST" action="{{ route('admin.reports.store') }}" target="_blank" id="generate-form">
                @csrf
                <button class="btn btn-secondary" type="submit">Генериши</button>
            </form>
            <form method="POST" action="{{ route('admin.reports.archive') }}" onsubmit="$('#archive-modal').modal('toggle')" id="archive-form">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger ml-2"
                        type="button"
                        data-toggle="modal"
                        data-target="#archive-modal"
                >Архивирај</button>
                @include('layouts.password_confirm_modal', [
                        'name' => 'archive-modal',
                        'title' => 'Архивирај анкету',
                        'warning' => 'Архивирање анкете ће избрисати све одговоре и кодове из базе и сачувати извештај у .docx формату.',
                        'text' => 'Унеси своју лозинку како би архивирао анкету.',
                        'action' => 'Архивирај',
                        'cancel' => 'Одустани',
                ])
            </form>
            <form method="POST" action="{{ route('admin.reports.reset') }}" id="reset-form" onsubmit="$('#reset-modal').modal('toggle')">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger ml-2"
                        type="button"
                        data-toggle="modal"
                        data-target="#reset-modal"
                >Ресетуј</button>
                @include('layouts.password_confirm_modal', [
                        'name' => 'reset-modal',
                        'title' => 'Ресетуј анкету',
                        'warning' => 'Ресетовање анкете ће избрисати све одговоре и кодове и НЕЋЕ сачувати извештај.',
                        'text' => 'Унеси своју лозинку како би ресетовао анкету.',
                        'action' => 'Ресетуј',
                        'cancel' => 'Одустани',
                ])
            </form>
        </div>

        <table class="table">
            <thead class="thead-dark">
                <th scope="col">
                    Извештај
                </th>
                <th scope="col">
                    Опције
                </th>
            </thead>
            <tbody>
                @foreach ($reports as $reportName)
                    <tr>
                        <th class="align-middle">{{ $reportName }}</th>
                        <td class="align-middle">
                            <a
                                    class="btn btn-secondary"
                                    href="{{ route('admin.reports.show', $reportName) }}"
                                    data-toggle="tooltip"
                                    data-placement="bottom"
                                    title="Преузми"
                            >
                                <i class="fa fa-download" style="color: #80ff1f"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
