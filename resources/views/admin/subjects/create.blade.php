@extends('layouts.admin')

@section('title', 'Додај предмет')

@section('content')
    <div class="container">
        <div class="h1 text-center">
            Додај предмет
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <form method="POST"action="{{ route('admin.departments.subjects.store', $department->id) }}">
                    @csrf

                    @include('admin.subjects.partials.fields')

                    <div class="d-flex justify-content-center">
                        @include('admin.partials.form_actions', [
                            'submitCta' => 'Додај',
                            'cancelUrl' => route('admin.departments.show', $department->id),
                            'cancelCta' => 'Откажи',
                        ])
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
