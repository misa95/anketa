@include('layouts.fields.input', [
    'name'      => 'name',
    'label'     => 'Име предмета',
    'value'     => $subject->name,
    'autofocus' => true,
])

@include('layouts.fields.input', [
    'name'  => 'semester',
    'type'  => 'number',
    'label' => 'Семестар',
    'value' => $subject->semester,

])

@include('layouts.fields.dropdown', [
    'name'       => 'teacher_id',
    'label'      => 'Професор',
    'options'    => $teachers,
    'selected'   => $subject->teacher_id,
    'placeholder' => 'Професор',
])

@include('layouts.fields.dropdown', [
    'name'     => 'assistant_id',
    'label'    => 'Асистент',
    'options'  => [null => 'Нема асистента'] + $teachers,
    'selected' => $subject->assistant_id,
    'placeholder' => 'Асистент',
])

@include('layouts.fields.input', [
    'name'     => 'department',
    'label'    => 'Смер',
    'value'    => $subject->department->name,
    'disabled' => true,
])
