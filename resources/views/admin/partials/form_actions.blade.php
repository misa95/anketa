<button type="submit" class="btn btn-success mr-3">{{ $submitCta }}</button>

<a type="button" class="btn btn-danger" href="{{ $cancelUrl }}">{{ $cancelCta }}</a>