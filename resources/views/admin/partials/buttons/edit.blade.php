<a
        class="btn btn-secondary"
        data-toggle="tooltip"
        data-placement="bottom"
        title="Измени"
        href="{{ $url }}"
>
    <i class="fa fa-pencil" style="color: #fff114"></i>
</a>
