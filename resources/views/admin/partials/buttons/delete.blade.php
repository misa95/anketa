<form action="{{ $url }}" method="POST">
    @csrf
    @method('DELETE')
    <button
            type="button"
            class="btn btn-secondary"
            data-toggle="modal"
            data-target="#{{ $modalId ?? "delete-{$url}-modal" }}"
            title="Izbriši"
    >
        <i class="fa fa-trash" style="color: #ff5e4e"></i>
    </button>
    @include('layouts.confirm_modal', [
        'name'   => $modalId ?? "delete-{$url}-modal",
        'title'  => $modalTitle ?? 'Потврди брисање',
        'text'   => $modalText ?? 'Потврди брисање',
        'cancel' => $cancelCta ?? 'Откажи',
        'action' => $confirmCta ?? 'Потврди'
    ])
</form>
