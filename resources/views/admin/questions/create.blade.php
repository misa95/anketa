@extends('layouts.admin')

@section('title', 'Додај питање')

@section('content')
    <div class="container">
        <div class="h1 text-center">
            Додај питање
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <form method="POST" action="{{ route('admin.questions.store') }}">
                    @csrf

                    @include('admin.questions.partials.fields')

                    <div class="d-flex justify-content-center">
                        @include('admin.partials.form_actions', [
                            'submitCta' => 'Додај',
                            'cancelUrl' => route('admin.questions.index'),
                            'cancelCta' => 'Откажи',
                        ])
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
