@include('layouts.fields.input', [
    'name'      => 'text',
    'label'     => 'Текст питања',
    'value'     => $question->text,
    'autofocus' => true,
])

@include('layouts.fields.dropdown', [
    'name'     => 'type',
    'label'    => 'Тип питања',
    'options'  => $typeOptions,
    'selected' => $question->type,
])
