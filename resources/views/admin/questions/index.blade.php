@extends('layouts.admin')

@section('title', 'Питање')

@section('content')
    <div class="container">
        <div class="col-md-10">
            <div class="d-flex justify-content-start">
                На сва питања се одговор даје у виду оцене од 1 до 5.
            </div>
            <div class="d-flex justify-content-end mb-3">
                <a class="btn btn-secondary mr-1" href="{{ route('admin.questions.create') }}">Додај питање</a>
            </div>

            <div class="mt-4">
                <ul class="nav nav-tabs" id="questions" role="tablist">

                    <li class="nav-item">
                        <a
                                class="nav-link active"
                                id="teacher-tab"
                                data-toggle="tab"
                                href="#teacher-questions"
                                role="tab"
                                aria-controls="teacher-questions"
                                aria-selected="true"
                        >Питања о професорима</a>
                    </li>

                    <li class="nav-item">
                        <a
                                class="nav-link"
                                id="subject-tab"
                                data-toggle="tab"
                                href="#subject-questions"
                                role="tab"
                                aria-controls="subject-questions"
                        >Питања о предметима</a>
                    </li>

                    <li class="nav-item">
                        <a
                                class="nav-link"
                                id="other-tab"
                                data-toggle="tab"
                                href="#other-questions"
                                role="tab"
                                aria-controls="other-questions"
                        >Питања о школи</a>
                    </li>
                </ul>

                <div class="tab-content mt-4">
                    <div class="tab-pane active" id="teacher-questions" role="tabpanel" aria-labelledby="teacher-tab">
                        <table class="table">
                            <thead class="thead-dark">
                            <th scope="col">
                                Питање
                            </th>
                            <th scope="col">
                                Опције
                            </th>
                            </thead>
                            <tbody>
                            @foreach ($questions->get('teacher') ?? [] as $question)
                                <tr>
                                    <th class="align-middle" scope="row">{{ $question->text }}</th>
                                    <td>
                                        <div class="row">
                                            <div class="mr-1">
                                                @include('admin.partials.buttons.delete', [
                                                    'url' => route('admin.questions.destroy', $question->id),
                                                    'modalId' => "delete-{$question->id}-modal",
                                                    'modalTitle' => 'Брисање питања',
                                                    'modalText' => "Потврди брисање питања \"{$question->text}\"",
                                                ])
                                            </div>
                                            @include('admin.partials.buttons.edit', ['url' => route('admin.questions.edit', $question->id)])
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="subject-questions" role="tabpanel" aria-labelledby="subject-tab">
                        <table class="table">
                            <thead class="thead-dark">
                            <th scope="col">
                                Питање
                            </th>
                            <th scope="col">
                                Опције
                            </th>
                            </thead>
                            <tbody>
                            @foreach ($questions->get('subject') ?? [] as $question)
                                <tr>
                                    <th class="align-middle" scope="row">{{ $question->text }}</th>
                                    <td>
                                        <div class="row">
                                            <div class="mr-1">
                                                @include('admin.partials.buttons.delete', [
                                                    'url' => route('admin.questions.destroy', $question->id),
                                                    'modalId' => "delete-{$question->id}-modal",
                                                    'modalTitle' => 'Брисање питања',
                                                    'modalText' => "Потврди брисање питања \"{$question->text}\"",
                                                ])
                                            </div>
                                            @include('admin.partials.buttons.edit', ['url' => route('admin.questions.edit', $question->id)])
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="other-questions" role="tabpanel" aria-labelledby="other-tab">
                        <table class="table">
                            <thead class="thead-dark">
                            <th scope="col">
                                Питање
                            </th>
                            <th scope="col">
                                Опције
                            </th>
                            </thead>
                            <tbody>
                            @foreach ($questions->get('other') ?? [] as $question)
                                <tr>
                                    <th class="align-middle" scope="row">{{ $question->text }}</th>
                                    <td>
                                        <div class="row">
                                            <div class="mr-1">
                                                @include('admin.partials.buttons.delete', [
                                                    'url' => route('admin.questions.destroy', $question->id),
                                                    'modalId' => "delete-{$question->id}-modal",
                                                    'modalTitle' => 'Брисање питања',
                                                    'modalText' => "Потврди брисање питања \"{$question->text}\"",
                                                ])
                                            </div>
                                            @include('admin.partials.buttons.edit', ['url' => route('admin.questions.edit', $question->id)])
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
