@extends('layouts.admin')

@section('title', 'Измени питање')

@section('content')
    <div class="container">
        <div class="h1 text-center">
            {{ $question->text }}
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <form method="POST" action="{{ route('admin.questions.update', $question->id) }}">
                    @csrf
                    @method('PATCH')

                    @include('admin.questions.partials.fields')

                    <div class="d-flex justify-content-center">
                        @include('admin.partials.form_actions', [
                            'submitCta' => 'Сачувај',
                            'cancelUrl' => route('admin.questions.index'),
                            'cancelCta' => 'Откажи',
                        ])
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
