@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Анкета</div>

                <div class="card-body">
                    <div class="text-center">
                        <div class="h3">Смер</div>
                        {{ $department->name }}
                        <div class="h3 mt-4">Семестар</div>
                        {{ Auth::user()->semester }}
                    </div>

                    <div class="d-flex justify-content-center mt-5">
                        <form method="POST" action="{{ route('survey.index') }}">
                            @csrf
                            <button class="btn btn-success mr-2" type="submit">Попуни анкету</button>
                        </form>

                        <form method="POST" action="{{ route('logout')  }}">
                            @csrf
                            <button class="btn btn-danger" type="submit">Одустани</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
