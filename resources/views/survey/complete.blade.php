@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Анкета</div>

                    <div class="card-body">
                        <div class="text-center h2">
                            Хвала на издвојеном времену,
                        </div>
                        <div class="text-center h4">
                            Твоји одговори су сачувани.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
