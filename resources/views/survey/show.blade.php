@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center h3 sticky-top">{{ $question->text }} ({{ $questionOrder }}/{{ $questionCount }})</div>

                        <form action="{{ route('survey.store', $question->id) }}" method="POST">
                            @csrf
                            <div class="card-body">
                                @if($question->isTeacher())
                                    @include('survey.partials.teacher_question_fields', [
                                        'subjects' => $subjects,
                                        'answers'  => $answers,
                                    ])
                                @endif

                                @if($question->isSubject())
                                    @include('survey.partials.subject_question_fields', [
                                        'subjects' => $subjects,
                                        'answers'  => $answers,
                                    ])
                                @endif

                                @if($question->isOther())
                                    @include('survey.partials.other_question_fields', [
                                        'answers' => $answers
                                    ])
                                @endif
                            </div>

                        <div class="card-footer">
                            @if(!$isFirst)
                            <a class="btn btn-primary" href="{{ route('survey.show', $previousQuestion->id) }}">
                                Назад
                            </a>
                            @endif
                            <button class="btn btn-success" type="submit">
                                @if($isLast)
                                    Заврши
                                @else
                                    Даље
                                @endif
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
