@foreach(range(1, 5) as $mark)
        <div class="form-check form-check-inline">
                <input class="form-check-input form-control-lg" type="radio" name="{{ $name }}" id="{{ "{$name}-{$mark}" }}" value="{{ $mark }}" @if(old($name, $checked ?? null) == $mark) checked @endif>
                <label class="form-check-label col-form-label-lg" for="{{ "{$name}-{$mark}" }}">{{ $mark }}</label>
        </div>
@endforeach
@if ($errors->has($name)) <label class="error-message col-md-12" for="{{ $name }}">{{ $errors->first($name) }}</label> @endif
