@foreach($subjects as $subject)
    <div class="row">
        <div class="col-md-4">
            {{ $subject->name }}
        </div>
        <div class="col-md-8">
            @include('survey.partials.mark_radios', [
                'name' => "subject-{$subject->id}",
                'checked' => $answers->get($subject->id),
            ])
        </div>
    </div>
    <hr>
@endforeach
