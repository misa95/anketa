@foreach ($subjects as $subject)
    <div class="row">
        <div class="col text-center h4">
            {{ $subject->name }}
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-4">
            {{ $subject->teacher->name }}
        </div>
        <div class="col-md-8">
            @include('survey.partials.mark_radios', [
                'name' => "subject-{$subject->id}-teacher-{$subject->teacher_id}",
                'checked' => $answers->get($subject->id)[$subject->teacher_id] ?? null,
            ])
        </div>
    </div>

    @if($subject->assistant)
        <div class="row mt-3">
            <div class="col-md-4">
                {{ $subject->assistant->name }}
            </div>
            <div class="col-md-8">
                @include('survey.partials.mark_radios', [
                    'name' => "subject-{$subject->id}-assistant-{$subject->assistant_id}",
                    'checked' => $answers->get($subject->id)[$subject->assistant_id] ?? null,
                ])
            </div>
        </div>
    @endif
    <hr>
@endforeach
