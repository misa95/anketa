<div class="row justify-content-center">
    @include('survey.partials.mark_radios', [
        'name' => 'mark',
        'checked' => $answers->get("mark"),
    ])
</div>