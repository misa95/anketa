@php
    /** @var \Illuminate\Support\Collection|\App\Models\Department[] $departments */
    /** @var \App\Models\Department $department */
    /** @var \Illuminate\Support\Collection|\App\Models\Code[] $departmentCodes */
@endphp
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h1>
                Кодови за попуњавање анкете
            </h1>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-2">
                @include('admin.departments.navigation', [
                    'departments' => $departments,
                    'activeDepartment' => $department->id,
                    'routeLink' => 'codes.show',
                ])
            </div>

            <div class="col-md-10">
                <ul class="nav nav-tabs" id="semesters" role="tablist">
                    @foreach($departmentCodes as $semester => $semesterCodes)
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                id="{{ $semester }}-tab"
                                data-toggle="tab"
                                href="#semester-{{ $semester }}"
                                role="tab"
                                aria-controls="semester-{{ $semester }}"
                                @if ($loop->first)
                                aria-selected="true"
                                @endif
                            >{{ $semester }}. семестар</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content mt-4">
                    @foreach($departmentCodes as $semester => $semesterCodes)
                        <div class="tab-pane" id="semester-{{ $semester }}" role="tabpanel" aria-labelledby="{{ $semester }}-tab">
                            <a class="btn btn-secondary float-right mb-3" href="{{ route('login') }}">Започни анкету</a>

                            <table class="table table-bordered content-center">
                                <thead class="thead-dark">
                                <th scope="col" colspan="3" class="text-center">
                                    Кодови
                                </th>
                                </thead>
                                <tbody>
                                @foreach ($semesterCodes->chunk(3) as $codesChunk)
                                    <tr>
                                        @foreach($codesChunk as $code)
                                            <td class="text-center">{{ $code->code }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
