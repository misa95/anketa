<?php

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartmentSeeder extends Seeder
{
    protected $departments = [
        [
            'name'      => 'Савремене рачунарске технологије',
            'shorthand' => 'СРТ'
        ],
        [
            'name'      => 'Комуникационе технологије',
            'shorthand' => 'KOT'
        ],
        [
            'name'      => 'Грађевинско инжењерство',
            'shorthand' => 'ГРИ'
        ],
        [
            'name'      => 'Индустријско инжењерство',
            'shorthand' => 'ИНИ'
        ],
        [
            'name'      => 'Заштита животне средине',
            'shorthand' => 'ЗЖС'
        ],
        [
            'name'      => 'Друмски саобраћај',
            'shorthand' => 'ДРС'
        ],
        [
            'name'      => 'Мултимедијалне комуникационе техноллогије',
            'shorthand' => 'МКТ'
        ],
        [
            'name'      => 'Управљање отпадом',
            'shorthand' => 'УО'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->departments as $department) {
            Department::firstOrCreate($department);
        }
    }
}
