<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DepartmentSeeder::class);
        $this->call(QuestionSeeder::class);

        if (app()->environment('local')) {
            $this->call(TeacherSeeder::class);
            $this->call(SubjectSeeder::class);
            $this->call(CodeSeeder::class);
            $this->call(AnswerSheetSeeder::class);
            $this->call(AnswerSeeder::class);
        }
    }
}
