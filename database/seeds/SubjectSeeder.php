<?php

use Illuminate\Database\Seeder;
use App\Models\Department;
use App\Models\Teacher;
use App\Models\Subject;
use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = Department::all();
        $teachers = Teacher::all();

        $departments->each(function (Department $department) use ($teachers) {
            $subjects = collect([]);

            for ($i = 0; $i < rand(6, 36); $i++) {
                $subjects->push($subject = factory(Subject::class)->make());
                $subject->department_id = $department->id;
                $subject->teacher_id = $teachers->random()->id;
                $subject->assistant_id = (rand(0, 1))
                    ? $teachers->random()->id
                    : null;
            }

            DB::table('subjects')->insert($subjects->toArray());
        });
    }
}
