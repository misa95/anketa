<?php

use App\Models\Question;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionSeeder extends Seeder
{
    protected $questions = [
        Question::TYPE_TEACHER => [
            'Наставник/сарадник је објективно оцењивао мој рад',
            'Наставник/сарадник потпуно образлаже градиво',
            'Наставник/сарадник подстиче активности у току наставе',
            'Наставник/сарадник инсистира на повезивању градива са примерима из праксе',
            'Наставник/сарадник ме мотивише и инспирише на рад',
            'Комуникација са наставником/сарадником је одговарајућа',
        ],
        Question::TYPE_SUBJECT => [
            'Литература за овај предмет је доступна',
            'Вежбе ми користе у праћењу предмета',
            'Предиспитне, испитне обавезе и начин бодовања су јасно дефинисани',
            'Предмет је допринео да боље разумем проблеме које покрива',
        ],
        Question::TYPE_OTHER => [
            'Задовољан/а сам радом студентске службе',
            'Задовољан/а сам радом библиотеке',
            'Задовољан/а сам радом студентског парламента',
            'Задовољан/а сам техничком опремљеношћу лабораторија',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = [];

        foreach ($this->questions as $type => $texts) {
            foreach ($texts as $text) {
                $questions[] = [
                    'type' => $type,
                    'text' => $text,
                ];
            }
        }

        DB::table('questions')->insert($questions);
    }
}
