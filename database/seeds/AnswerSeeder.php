<?php

use App\Models\AnswerSheet;
use App\Models\Department;
use App\Models\Question;
use App\Models\Subject;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class AnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = Question::all()->groupBy('type');
        $departments = Department::with('subjects.teacher', 'subjects.assistant')
                                 ->get()
                                 ->keyBy('id');

        $answers = collect();

        foreach (AnswerSheet::all() as $answerSheet) {
            $subjects = $departments
                            ->get($answerSheet->department_id)
                            ->subjects
                            ->where('semester', $answerSheet->semester);

            $answers = $answers->concat(
                $questions->get(Question::TYPE_OTHER)
                          ->map(function (Question $question) use ($answerSheet) {
                              return [
                                  'answer_sheet_id' => $answerSheet->id,
                                  'question_id' => $question->id,
                                  'subject_id' => null,
                                  'teacher_id' => null,
                                  'mark' => rand(1, 5),
                              ];
                          })
            );

            $answers = $answers->concat(
                $questions->get(Question::TYPE_SUBJECT)->pluck('id')->crossJoin(
                    $subjects->pluck('id')
                )->map(function (array $ids) use ($answerSheet) {
                    return [
                        'answer_sheet_id' => $answerSheet->id,
                        'question_id' => $ids[0],
                        'subject_id' => $ids[1],
                        'teacher_id' => null,
                        'mark' => rand(1, 5),
                    ];
                })
            );

            $answers = $answers->concat(
                $questions->get(Question::TYPE_TEACHER)->pluck('id')->crossJoin(
                    $subjects
                )->map(function (array $questionIdAndSubject) use ($answerSheet) {
                    $subject = $questionIdAndSubject[1];
                    $answers = [
                        [
                            'answer_sheet_id' => $answerSheet->id,
                            'question_id' => $questionIdAndSubject[0],
                            'subject_id' => $subject->id,
                            'teacher_id' => $subject->teacher_id,
                            'mark' => rand(1, 5),
                        ]
                    ];

                    if ($subject->assistant_id !== null ) {
                        $answers[] = [
                            'answer_sheet_id' => $answerSheet->id,
                            'question_id' => $questionIdAndSubject[0],
                            'subject_id' => $subject->id,
                            'teacher_id' => $subject->assistant_id,
                            'mark' => rand(1, 5),
                        ];
                    }

                    return $answers;
                })->flatten(1)
            );
        }

        DB::transaction(function () use ($answers) {
            $answers->chunk(1000)->each(function (Collection $chunk) {
                DB::table('answers')->insert($chunk->toArray());
            });
        });
    }
}
