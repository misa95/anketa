<?php

use App\Models\Department;
use Illuminate\Database\Seeder;

class AnswerSheetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::all()->each(function (Department $department) {
            foreach ($department->availableSemesters() as $semester) {
                DB::table('answer_sheets')->insert(
                    array_fill(0, rand(10, 50), [
                        'department_id' => $department->id,
                        'semester' => $semester,
                        'completed_on' => now()->toDateTimeString(),
                    ])
                );
            }
        });
    }
}
