<?php

use App\Models\Department;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $codes = collect([]);


        foreach (Department::all() as $department) {
            foreach ($department->availableSemesters() as $semester) {
                $codes = $codes->merge(collect(range(1, rand(2, 10)))->transform(function () use ($department, $semester) {
                    return [
                        'code' => \Illuminate\Support\Str::random(10),
                        'department_id' => $department->id,
                        'semester' => $semester,
                    ];
                }));
            }
        }

        DB::table('codes')->insert($codes->toArray());
    }
}
