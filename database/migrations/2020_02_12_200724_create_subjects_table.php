<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');

            $table->unsignedInteger('department_id');
            $table->foreign('department_id')
                  ->references('id')
                  ->on('departments')
                  ->onDelete('cascade');

            $table->smallInteger('semester');

            $table->unsignedInteger('teacher_id');
            $table->foreign('teacher_id')
                  ->references('id')
                  ->on('teachers');

            $table->unsignedInteger('assistant_id')->nullable();
            $table->foreign('assistant_id')
                  ->references('id')
                  ->on('teachers');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
