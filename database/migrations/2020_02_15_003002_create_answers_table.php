<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('answer_sheet_id')->unsigned();
            $table->foreign('answer_sheet_id')
                  ->references('id')
                  ->on('answer_sheets');

            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')
                  ->references('id')
                  ->on('questions');

            $table->integer('subject_id')->unsigned()->nullable();
            $table->foreign('subject_id')
                  ->references('id')
                  ->on('subjects');

            $table->integer('teacher_id')->unsigned()->nullable();
            $table->foreign('teacher_id')
                  ->references('id')
                  ->on('teachers');

            $table->integer('mark');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
