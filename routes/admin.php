<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/login');

Route::middleware('guest:admin')->group(function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('perform-login');
});

Route::middleware('auth:admin')->group(function () {
    Route::post('logout', 'LoginController@logout')->name('logout');

    Route::group(['active_item' => 'departments'], function () {
        Route::resource('departments', 'DepartmentController');

        Route::post('departments/{department}/import', 'DepartmentController@import')
             ->name('departments.import');

        Route::resource('departments.subjects', 'SubjectController')->except([
            'index', 'show'
        ])->shallow();
    });

    Route::group(['active_item' => 'questions'], function () {
        Route::resource('questions', 'QuestionController')->except('show');
    });

    Route::group(['active_item' => 'teachers'], function () {
        Route::resource('teachers', 'TeacherController');
    });

    Route::group(['active_item' => 'codes'], function () {
        Route::resource('departments.codes', 'CodeController')->only(
            'index', 'create', 'store'
        )->shallow();

        Route::get('/departments/{department}/codes/{semester}/print', 'CodeController@print')
             ->name('codes.print');

        Route::get('/departments/codes/first', 'CodeController@first')
             ->name('departments.codes.first');

        Route::post('/codes/publish', 'CodeController@publish')
            ->name('codes.publish');

        Route::post('/codes/hide', 'CodeController@hide')
            ->name('codes.hide');
    });

    Route::group(['active_item' => 'reports'], function () {
        Route::resource('reports', 'ReportController')
            ->only('index', 'show', 'store');

        Route::delete('/reports', 'ReportController@reset')
             ->name('reports.reset')
             ->middleware('password_protected');

        Route::delete('/reports/archive', 'ReportController@destroy')
             ->name('reports.archive')
             ->middleware('password_protected');
    });
});
