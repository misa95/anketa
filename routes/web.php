<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/home');

Route::get('/codes', 'CodesController@index')->name('codes.index');
Route::get('/codes/{department}', 'CodesController@show')->name('codes.show');

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::middleware('auth:web')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::post('/survey/begin', 'SurveyController@index')->name('survey.index');
    Route::get('/survey/take/{question}', 'SurveyController@show')->name('survey.show');
    Route::post('/survey/{question}/store', 'SurveyController@store')->name('survey.store');
    Route::get('/survey/complete', 'SurveyController@complete')->name('survey.complete');
});
